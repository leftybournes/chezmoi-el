;;; chezmoi.el --- Integrate emacs with chezmoi

;; Copyright (C) 2019  Kent Delante

;; This file is part of chezmoi-el.

;; chezmoi-el is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; chezmoi-el is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with chezmoi-el.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;; (defcustom chezmoi-dir "~/.local/share/chezmoi"
;;   "Chezmoi dotfiles directory."
;;   :group 'chezmoi
;;   :type 'string)

;; (defcustom chezmoi-config-dir "~/.config/chezmoi/chezmoi.toml"
;;   "Chezmoi config directory."
;;   :group 'chezmoi
;;   :type 'string)

(defcustom chezmoi-add-file nil
  "File to add to chezmoi."
  :group 'chezmoi-add
  :type 'string)

(defcustom chezmoi-add-as-template nil
  "Whether to add chosen file as chezmoi template."
  :group 'chezmoi-add
  :type 'string)

(defun chezmoi-add ()
  "Invoke the `chezmoi add` command to chosen file/directory."
  (interactive)
  (setq chezmoi-add-file (read-string "Add file/directory: "))
  (when (file-regular-p chezmoi-add-file)
         (while (progn
                  (setq chezmoi-add-as-template
                        (read-string "Add as template? (yes or no) "))
                  (not (or (string-equal chezmoi-add-as-template "yes")
                           (string-equal chezmoi-add-as-template "no"))))))
  (let ((default-directory "~/"))
    (if (string-equal chezmoi-add-as-template "yes")
        (progn (shell-command (concat "chezmoi add -T " chezmoi-add-file))
               (princ (concat "Added " chezmoi-add-file " as template.")))
      (progn (shell-command (concat "chezmoi add " chezmoi-add-file))
             (princ (concat "Added " chezmoi-add-file "."))))))

(defun chezmoi-apply ()
  "Invoke the `chezmoi apply` command."
  (interactive)
  (if (string-equal (shell-command-to-string "chezmoi apply") "")
      (princ "Changes applied")))
  

;; (defcustom chezmoi-edit-file nil
;;   "File to edit."
;;   :group 'chezmoi-edit
;;   :type 'string)

;; (defun chezmoi-edit ()
;;   "Edit specified file if it has been added to chezmoi."
;;   (interactive)
;;   (setq chezmoi-edit-file
;;         (read-string "Edit file: "))
;;   (let ((default-directory "~/"))
;;     (if (file-exists-p chezmoi-edit-file)
;;         (progn ())
;;       (princ "File doesn't exist."))))

;; (defun chezmoi-config-edit ()
;;   "Edit chezmoi config.")

(provide 'chezmoi)
;;; chezmoi.el ends here
